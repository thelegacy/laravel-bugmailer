<?php

namespace Bugmail;

use Exception;
use LimitIterator;
use SplFileObject;
use Illuminate\Support\Arr;
use Symfony\Component\Debug\Exception\FlattenException;

class Formatter
{
    protected $charset;

    protected $exception;

    protected $tables = [];

    protected $sysTables = [];

    protected $shortTrace = true;

    protected $longFileNames = false;

    public function __construct (FlattenException $exception, $charset = null, $fileLinkFormat = null)
    {
        $this->exception = $exception;
        $this->charset = $charset ?: ini_get('default_charset') ?: 'UTF-8';
    }

    public function shortTrace ($value)
    {
        $this->shortTrace = (bool) $value;
        return $this;
    }

    public function longFileNames ($value)
    {
        $this->longFileNames = (bool) $value;
        return $this;
    }


    public function sysTable ($name, $value)
    {
        $this->sysTables[$name] = $value;
        return $this;
    }

    public function table ($name, $value)
    {
        $this->tables[$name] = $value;
        return $this;
    }

    public function render ()
    {
        $content = $this->content($this->exception);
        $css = $this->stylesheet($this->exception);
        $title = $this->title($this->exception);

        return ('<!DOCTYPE html>
<html>
<head>
    <meta charset="' . $this->charset . '" />
    <meta name="robots" content="noindex,nofollow" />
    <title>' . $title . '</title>
    <style>' . $css . '</style>
</head>
<body>' . $content . '</body>
</html>');
    }

    public function title (FlattenException $exception)
    {
        return $exception->getClass() . ' - ' . $exception->getMessage();
    }

    public function isFrameworkPath ($path)
    {
        $vendorPath = base_path('vendor');
        $indexPath = public_path('index.php');
        if ($path == $indexPath) {
            return true;
        }
        if (strpos($path, $vendorPath) !== false) {
            return true;
        }
        return false;
    }

    public function content (FlattenException $exception)
    {
        $title = '';
        $content = '';
        $tables = '';

        $first = true;

        try {
            $count = count($exception->getAllPrevious());
            $total = $count + 1;

            foreach ($exception->toArray() as $position => $e) {
                $ind = $count - $position + 1;
                $class = $this->formatClass($e['class']);
                $message = nl2br($this->escapeHtml($e['message']));
                $content .= sprintf((
                '<h2 class="block_exception clear_fix" style="color: #333; padding:10px;border-bottom:1px solid #ccc;overflow: hidden;word-wrap: break-word;margin:0;margin-bottom:10px">
                        <span class="exception_counter" style="background-color: #fff; color: #333; padding: 6px; float: left; margin-right: 10px; float: left; display: block">%d/%d</span>
                        <span class="exception_title" style="margin-left: 2.5em; margin-bottom: 0.5em; display: block">%s%s:</span>
                        <span class="exception_message" style=" margin-left: 2.5em; display: block">%s</span>
                    </h2>'
                ), $ind, $total, $class, $this->formatPath($e['trace'][0]['file'], $e['trace'][0]['line']), $message);

                $shortTrace = $this->shortTrace;
                if ($shortTrace) {
                    $haveAppPath = false;
                    foreach ($e['trace'] as $trace) {
                        $file = $trace['file'];
                        if (is_null($file)) {
                            continue;
                        }
                        elseif ( ! $this->isFrameworkPath($file)) {
                            $haveAppPath = true;
                            break;
                        }
                    }
                    if ( ! $haveAppPath) {
                        $shortTrace = false;
                    }
                }

                foreach ($e['trace'] as $trace) {
                    $file = $trace['file'];

                    if ($shortTrace && ($this->isFrameworkPath($file) || is_null($file))) {
                        continue;
                    }

                    if ($first) {
                        $content .= $this->fileSnippet($e['trace'][0]['file'], $e['trace'][0]['line']);
                        $content .= '<div class="block">';
                        $content .= '<ol class="traces list_exception" style="padding-left:10px">';
                    }

                    $first = false;

                    $content .= '<li style="font-size:12px; padding: 2px 4px; list-style-type:decimal; margin-left:20px">';
                    if ($trace['function']) {
                        $content .= sprintf('at %s%s%s(%s)', $this->formatClass($trace['class']), $trace['type'],
                            $trace['function'], $this->formatArgs($trace['args']));
                    }
                    if (isset($trace['file']) && isset($trace['line'])) {
                        $content .= $this->formatPath($trace['file'], $trace['line']);
                    }
                    $content .= "</li>\n";
                }
                $content .= "</ol>\n</div>\n";
            }
        } catch (\Exception $e) {
            // something nasty happened and we cannot throw an exception anymore
            $title = sprintf('Exception thrown when handling an exception (%s: %s)', get_class($e),
                $this->escapeHtml($e->getMessage()));
        }

        if ($title) {
            $title = "<h1>$title</h1>";
        }

        $allTables = $this->tables + $this->sysTables;

        if ($allTables) {
            $tables .= '<div style="padding-left:10px;border-top:1px solid #ccc">';

            foreach ($allTables as $name => $data) {
                $tables .= '<h4 style="margin-bottom:0.5em">' . $name . '</h4>';

                if ($data) {
                    $isAssoc = Arr::isAssoc($data);
                    $tables .= '<table style="width:100%">';
                    foreach ($data as $key => $value) {
                        $tables .= '<tr>';

                        if ($isAssoc) {
                            $tables .= '<td style="font-size:12px;vertical-align: top;white-space: nowrap;">' . $this->escapeHtml($key) . '</td>';
                        }
                        $tables .= '<td style="font-size:12px;vertical-align: top">' . $this->dump($value) . '</td>';
                        $tables .= '</tr>';
                    }
                    $tables .= '</table>';
                } else {
                    $tables .= '<span style="color:#999;font-size:12px">empty</span>';
                }
            }
            $tables .= '</div>';
        }

        return (
            '<div id="sf-resetcontent" class="sf-reset" style="font-family:-apple-system,BlinkMacSystemFont,\'Segoe UI\',Roboto,Oxygen-Sans,Ubuntu,Cantarell,\'Helvetica Neue\',sans-serif;">' .
            $title .
            $content .
            $tables .
            '</div>'
        );
    }

    protected function formatClass ($class)
    {
        $parts = explode('\\', $class);
        return sprintf('<abbr title="%s">%s</abbr>', $class, array_pop($parts));
    }

    protected function escapeHtml ($str)
    {
        return htmlspecialchars($str, ENT_QUOTES | ENT_SUBSTITUTE, $this->charset);
    }

    protected function formatPath ($path, $line)
    {
        $path = str_replace(base_path() . '/', '', $path);
        $path = $this->escapeHtml($path);

        if ($this->longFileNames) {
            $file = $path;
        } else {
            $file = preg_match('#[^/\\\\]*$#', $path, $file) ? $file[0] : $path;
        }
        return sprintf(' in <abbr title="%s">%s</abbr> line %3$d', $path, $file, $line);
    }

    protected function fileSnippet ($file, $line)
    {
        if ( ! $file || ! file_exists($file)) {
            return '';
        }
        $file = new SplFileObject($file);
        $fileIterator = new LimitIterator($file, $line - 6, 11);
        $output = '';
        $lineNumber = $line - 5;
        $output .= '<table style="width:100%">';
        foreach ($fileIterator as $code) {
            if ($lineNumber == $line) {
                $output .= '<tr style="background-color:#f1362f;color:white">';
            } else {
                $output .= '<tr>';
            }
            $output .= '<td style="float:left;font-size:11px;padding:0 10px">' . ($lineNumber) . '</td>';
            $output .= '<td style="float:left"><pre style="margin:0;font-size:11px">' . $this->escapeHtml($code) . '</pre></td>';
            $output .= '</tr>';
            $lineNumber++;
        }
        $output .= '</table>';
        return $output;
    }

    protected function formatArgs (array $args)
    {
        $result = array();
        foreach ($args as $key => $item) {
            if ('object' === $item[0]) {
                $formattedValue = sprintf('<em>object</em>(%s)', $this->formatClass($item[1]));
            } elseif ('array' === $item[0]) {
                $formattedValue = sprintf('<em>array</em>(%s)',
                    is_array($item[1]) ? $this->formatArgs($item[1]) : $item[1]);
            } elseif ('string' === $item[0]) {
                $formattedValue = sprintf("'%s'", $this->escapeHtml($item[1]));
            } elseif ('null' === $item[0]) {
                $formattedValue = '<em>null</em>';
            } elseif ('boolean' === $item[0]) {
                $formattedValue = '<em>' . strtolower(var_export($item[1], true)) . '</em>';
            } elseif ('resource' === $item[0]) {
                $formattedValue = '<em>resource</em>';
            } else {
                $formattedValue = str_replace("\n", '', var_export($this->escapeHtml((string) $item[1]), true));
            }

            $result[] = is_int($key) ? $formattedValue : sprintf("'%s' => %s", $key, $formattedValue);
        }

        return implode(', ', $result);
    }

    protected function dump ($value)
    {
        if (is_string($value)) {
            return $value;
        } elseif (is_null($value)) {
            return '<span style="color:#999;font-size:12px">null</span>';
        }
        return var_export($value, true);
    }

    protected function stylesheet (FlattenException $exception)
    {
        return 'html {width:600px;margin:auto}body{margin:0}';
    }


}
