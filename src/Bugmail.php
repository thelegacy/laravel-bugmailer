<?php

namespace Bugmail;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Exception\FlattenException;

class Bugmail extends Formatter
{
    public static function format($exception)
    {
        if (! $exception instanceof FlattenException) {
            $exception = FlattenException::create($exception);
        }

        $formatter = new self($exception);

        /** @var $request Request */
        $request = app('request');
        $session = app('session');

        $tables = [
            'Application' => value(function () use ($request) {
                $user = $request->user();

                if ($user) {
                    if (method_exists($user, 'identity')) {
                        $user = $user->identity();
                    } elseif (method_exists($user, 'exceptionDisplay')) {
                        $user = $user->exceptionDisplay();
                    } elseif (method_exists($user, 'toArray')) {
                        $user = $user->toArray();
                    }
                }

                return [
                    'Date' => with(new Carbon())->toDateTimeString(),
                    'User' => $user,
                    'User Agent' => $request->header('user-agent', 'unknown'),
                    'IP' => $request->ip() ?: 'unknown',
                    'Environment' => app()->environment(),
                    'SAPI' => php_sapi_name(),
                ];
            })
        ];

        $tables['Route'] = value(function () use ($request) {
            if ($route = $request->route()) {
                return [
                    'Uri' => $route->uri(),
                    'Parameters' => $route->parameters(),
                    'Action' => $route->getActionName(),
                    'Name' => $route->getName(),
                    'Full url' => $request->fullUrl(),
                    'Methods' => $route->methods(),
                ];
            }
        });

        if ($queries = app('db')->getQueryLog()) {
            $tables['Query log'] = $queries;
        }

        $tables = $tables + [
            'GET Data' => $request->query(),
            'POST Data' => $_POST,
            'JSON Data' => value(function () {
                $value = $request->json();

                if ($value instanceof ParameterBag) {
                    return $value->all();
                }
                return $value;
            }),
            'Cookies' => $request->cookie(),
            'Session' => $session->all(),
            'Files' => $request->file(),
            'Headers' => value(function () use ($request) {
                $keys = array_keys($request->headers->all());
                $headers = [];
                foreach ($keys as $key) {
                    $headers[$key] = $request->headers->get($key);
                }

                return $headers;
            }),
            'Server/Request Data' => $request->server(),
            'Environment Variables' => value(function () {
                $vars = [];
                foreach ($_ENV as $k => $v) {
                    $lower = strtolower($k);
                    if (strpos($lower, 'pass') !== false || $lower == 'app_key') {
                        $v = str_repeat('*', strlen($v));
                    }
                    $vars[$k] = $v;
                }

                return $vars;
            }),
        ];

        foreach ($tables as $key => $value) {
            $formatter->sysTable($key, $value);
        }

        return $formatter;
    }

    public function send(callable $fn)
    {
        $mailer = app('mailer');
        $content = $this->render();

        return $mailer->send([], [], function ($message) use ($fn, $content) {
            $message->subject($this->emailSubject());
            $message->setBody($content, 'text/html');

            call_user_func($fn, $message);
        });
    }

    protected function emailSubject()
    {
        $name = config('app.name');
        if (! $name) {
            $name = env('APP_NAME');
        }
        if (! $name) {
            $name = basename(base_path());
        }
        $env = app()->environment();

        return '['.$name.':'.$env.'] '.$this->title($this->exception);
    }
}
