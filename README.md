# Bugmail - Exception mailer for Laravel

Usage:

```php
<?php

use Bugmail\Bugmail;

...

Bugmail::format($exception)

       // Optional - disable short stack trace
       ->shortTrace(false)
   
       // Optional - use longer file paths
       ->longFileNames(true)

       // Optional - add table section to report
       ->table('User Info', ['id' => 7, 'name' => 'Laravel'])

       ->send(function($message) {
           // Usage the same as mailer closure
           $message->to('support@mydomain.com');
           ...
       });

```

To send report conditionally, use your own conditionals, like:

```php
<?php

use Bugmail\Bugmail;

...

if ($app->environment('production', 'staging')) {
    Bugmail::format($exception)->send(function($message) {
        ...
    });
}

```

Bugmail is easiest to implement in **App\Exception\Handler@report** method.

```
<?php

namespace App\Exceptions;

use Bugmail\Bugmail;
...

class Handler extends ExceptionHandler
{
    ...
    public function report(Exception $e)
    {
        parent::report($e);

        Bugmail::format($e)->send(...)
    }
...
```
